import React from "react";
import "../css/character.scss";
import { withRouter, RouteComponentProps } from "react-router";
import {
  characters,
  elements_images,
  weapon_types_images,
} from "./characters_list";
import LevelsContainer from '../components/levels_container';
import TalentsContainer from './talents_container';
import WeaponContainer from './weapon_container';
import ArtifactsContainer from './artifacts_container';
import { resource } from 'react-suspense-img';
import { getColorFromURL } from "color-thief-node";
import { Helmet } from "react-helmet";
import NotFound from '../pages/404'
import genshindb from '../libraries/genshindb.js';
import StatsDmgContainer from "./stats_dmg_container";
import { CryptoJS } from 'node-cryptojs-aes';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExport } from '@fortawesome/free-solid-svg-icons';
import ReactTooltip from 'react-tooltip'

declare const window: any;

class Character extends React.Component<RouteComponentProps> {
  constructor(props) {
    super(props)
  }

  state = {
    level: 20,
    ascend: 0,
    auto: {
      value: 1,
      label: '1'
    },
    e: {
      value: 1,
      label: '1'
    },
    q: {
      value: 1,
      label: '1'
    },
    weapon: {
      name: '',
      level: 20,
      ascend: 0
    },
    artifacts: {
      atk: 0,
      atk_p: 0,
      hp: 0,
      hp_p: 0,
      def: 0,
      def_p: 0,
      crit_rate: 0,
      crit_dmg: 0,
      phy_dmg: 0,
      ele_dmg: 0,
      ele_m: 0,
      energy_rc: 0
    }
  };

  isvalid = Object.keys(characters).includes(window.location.href.split("?")[1])
  character_image = this.isvalid ? genshindb.character(this.props.location.search.split("?")[1]).images.icon : ''
  character = this.props.location.search.split("?")[1];
  element = this.isvalid ? weapon_types_images[characters[this.character].weapon] : ''
  weapon_type = this.isvalid ? elements_images[characters[this.character].element] : ''
  auto = this.isvalid ? genshindb.talents(this.character).images.combat1 : ''
  e = this.isvalid ? genshindb.talents(this.character).images.combat2 : ''
  q = this.isvalid ? genshindb.talents(this.character).images.combat3 : ''
  p1 = this.isvalid ? genshindb.talents(this.character).images.passive1 : ''
  p2 = this.isvalid ? genshindb.talents(this.character).images.passive2 : ''
  up = this.isvalid ? genshindb.talents(this.character).images.passive3 : ''
  preload_all = this.isvalid ? [resource.preloadImage(this.auto), resource.preloadImage(this.e), resource.preloadImage(this.q), resource.preloadImage(this.p1), resource.preloadImage(this.p2), resource.preloadImage(this.up)] : ''

  noAscend = (level) => {
    switch (level) {
      case "20":
        return 0;
      case "40":
        return 1;
      case "50":
        return 2;
      case "60":
        return 3;
      case "70":
        return 4;
      case "80":
        return 5;
      case "90":
        return 6;
    }
  }

  ascends = (level) => {
    switch (level) {
      case "20":
        return 1;
      case "40":
        return 2;
      case "50":
        return 3;
      case "60":
        return 4;
      case "70":
        return 5;
      case "80":
        return 6;
    }
  };

  chooseLevel = (e, btns) => {
    window.gtag("event", "character_level", {
      event_category: "Character_Level",
    });
    let ascend = 0;
    btns.forEach(b => {
      let bt = b as HTMLElement
      bt.style.boxShadow = 'none'
      bt.style.backgroundColor = '#1e1c29'
    })
    let btn = e.currentTarget as HTMLElement
    btn.style.boxShadow = '0 0 0 0.2rem gray'
    btn.style.backgroundColor = '#23272b'
    if (e.currentTarget.childNodes[1]) {
      ascend = this.ascends(e.currentTarget.firstChild.nodeValue)
    }
    else {
      ascend = this.noAscend(e.currentTarget.firstChild.nodeValue)
    }
    this.setState({
      level: parseInt(e.currentTarget.firstChild.nodeValue),
      ascend: ascend
    });
  };

  chooseWeaponLevel = (e, btns) => {
    window.gtag("event", "weapon_level", {
      event_category: "Weapon_Level",
    });
    let ascend = 0;
    btns.forEach(b => {
      let bt = b as HTMLElement
      bt.style.boxShadow = 'none'
      bt.style.backgroundColor = '#1e1c29'
    })
    let btn = e.currentTarget as HTMLElement
    btn.style.boxShadow = '0 0 0 0.2rem gray'
    btn.style.backgroundColor = '#23272b'
    if (e.currentTarget.childNodes[1]) {
      ascend = this.ascends(e.currentTarget.firstChild.nodeValue)
    }
    else {
      ascend = this.noAscend(e.currentTarget.firstChild.nodeValue)
    }
    let weapon_details = {
      name: this.state.weapon.name,
      level: parseInt(e.currentTarget.firstChild.nodeValue),
      ascend: ascend
    }
    this.setState({ weapon: weapon_details })
  };

  chooseWeapon = (name) => {
    window.gtag("event", "weapon", {
      event_category: "Weapons",
    });
    let weapon_details = {
      name: name,
      level: this.state.weapon.level,
      ascend: this.state.weapon.ascend,
    }
    this.setState({ weapon: weapon_details })
  }

  chooseTalent = (talent, level) => {
    window.gtag("event", talent, {
      event_category: "Talents",
    });
    let talents = {}
    talents[talent] = level
    this.setState(talents)
  }

  artifactsDetails = (inputs) => {
    window.gtag("event", "calc_artifacts", {
      event_category: "Calc_Artifacts",
    });
    let inputs_values = []
    let details = {}
    let artifact_details = ['atk', 'atk_p', 'hp', 'hp_p', 'def', 'def_p', 'crit_rate', 'crit_dmg', 'phy_dmg', 'ele_dmg', 'ele_m', 'energy_rc']
    inputs.forEach(input => {
      inputs_values.push(parseFloat(input.value))
    })
    artifact_details.map((artifact, i) => {
      if (!isNaN(inputs_values[i])) {
        if (artifact.includes('_p')) {
          details[artifact] = inputs_values[i] / 100
        }
        else {
          details[artifact] = inputs_values[i]
        }
      }
      else {
        details[artifact] = this.state.artifacts[artifact]
      }
    })
    this.setState({
      artifacts: details
    })
  }

  exportCharacter = () => {
    window.gtag("event", "export", {
      event_category: "Export",
    });
    let et = document.querySelector('.export') as HTMLElement;
    ReactTooltip.show(et)
    let character = this.state
    let encrypted = CryptoJS.AES.encrypt(JSON.stringify(character), "pass").toString()
    window.history.replaceState(null, "", "/character?" + this.character + "?" + encrypted)
    navigator.clipboard.writeText(window.location.href)
  }

  importCharacter = () => {
    if (window.location.href.split("?").length == 3) {
      window.gtag("event", "import", {
        event_category: "Import",
      });
      let encrypted = window.location.href.split("?")[2]
      let decrypted = CryptoJS.AES.decrypt(encrypted, "pass")
      let data = CryptoJS.enc.Utf8.stringify(decrypted)
      this.setState(JSON.parse(data))
    }
  }

  importData = () => {
    if (window.location.href.split("?").length == 3) {
      let encrypted = window.location.href.split("?")[2]
      let decrypted = CryptoJS.AES.decrypt(encrypted, "pass")
      let data = CryptoJS.enc.Utf8.stringify(decrypted)
      return JSON.parse(data)
    }
    else {
      return ''
    }
  }

  yeetColor = async () => {
    let imageToBlob = require('image-to-blob')
    let DOMURL = window.URL || window.webkitURL || window;
    let dominantcolor = ""
    let appendBlob = async (err, blob) => {
      if (err) {
        console.error(err);
        return;
      }
      var img = new Image
      img.src = DOMURL.createObjectURL(blob);
      dominantcolor = await getColorFromURL(img.src);
      let color =
        "rgb(" +
        dominantcolor[0] +
        ", " +
        dominantcolor[1] +
        ", " +
        dominantcolor[2] +
        ")";
      document.documentElement.style.setProperty('--border', "solid 5px " + color)
      document.documentElement.style.setProperty('--color', color)
      document.documentElement.style.setProperty('--trans', 'scaleX(1)')
    }
    await imageToBlob(this.e, appendBlob);
  }

  componentDidMount() {
    this.yeetColor()
    this.importCharacter()
  }

  public render(): JSX.Element {
    if (!Object.keys(characters).includes(window.location.href.split("?")[1])) {
      return (
        <NotFound />
      )
    }
    return (
      <div className="stats_container">
        <Helmet>
          <meta charSet="utf-8" name='keywords' content='genshin apps calculator artifacts damage' />
          <title>{this.character.charAt(0).toUpperCase() + this.character.slice(1)}</title>
          <meta name="description" content="Genshin Impact damage calculator for character of your choice." />
          <meta property="og:image" content={this.character_image} />
          <link rel="canonical" href={"http://genshinapps.netlify.app/character?" + this.character} />
        </Helmet>
        <div className="character_details">
          <h2>{this.character.toUpperCase()}</h2>
          <div
            className="character_image"
            style={{ backgroundImage: "url(" + this.character_image + ")" }}
          >
          </div>
          <div className="element_weapon_container">
            <div data-tip data-for='export' className="btn export" onClick={this.exportCharacter.bind(this)}>
              <ReactTooltip
                id='export'
                type="dark"
                effect="solid"
                className='export_tooltip'
                arrowColor='rgba(16, 15, 20, 0.5)'
                offset={{ top: -5, left: 5 }}
                getContent={() => {
                  return (
                    'Exported & Copied!'
                  )
                }}
                afterShow={() => {
                  let et = document.querySelector('.export_tooltip') as HTMLElement;
                  let e = document.querySelector('.export') as HTMLElement;
                  et.style.opacity = '1'
                  setTimeout(() => {
                    et.style.opacity = '0'
                  }, 500)
                  setTimeout(() => {
                    ReactTooltip.hide(e)
                  }, 1000)
                }}
                event='none'
              />
              <FontAwesomeIcon size="2x" icon={faFileExport} />
            </div>
            <div
              className="element"
              style={{ backgroundImage: "url(" + this.element + ")" }}
            ></div>
            <div
              className="weapon_type"
              style={{ backgroundImage: "url(" + this.weapon_type + ")" }}
            ></div>
          </div>
          <div className="skills_container">
            <LevelsContainer choose={this.chooseLevel.bind(this)} imported_character={this.importData.bind(this)} />
            <TalentsContainer auto={this.auto} e={this.e} q={this.q} p1={this.p1} p2={this.p2} up={this.up} talentsLevels={this.chooseTalent.bind(this)} imported_character={this.importData.bind(this)} />
          </div>
        </div>
        <div className="weapon_artifacts_container">
          <WeaponContainer character={this.character} chooselevel={this.chooseWeaponLevel} choosename={this.chooseWeapon} element={this.element} imported_character={this.importData.bind(this)} />
          <ArtifactsContainer inputs={this.artifactsDetails.bind(this)} imported_character={this.importData.bind(this)} />
        </div>
        <StatsDmgContainer details={this.state} character={this.character} />
      </div>
    );
  }
}

export default withRouter(Character);
