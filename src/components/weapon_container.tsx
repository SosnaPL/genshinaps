import React from 'react';
import Star from "../public/star.png";
import '../css/weapon_container.scss'
import { weapon_style } from './select_styles';
import { characters } from "./characters_list";
import Select from 'react-select';
import genshindb from '../libraries/genshindb.js';
import { resource, Img } from 'react-suspense-img';

export default class WeaponContainer extends React.Component<{ character: string, chooselevel: Function, choosename: Function, element: string, imported_character: Function }> {

  constructor(props) {
    super(props);
  }

  weaponOptions = () => {
    let weapon_names = genshindb.weapons(characters[this.props.character].weapon, { matchCategories: true })
    let weapon_options = []
    weapon_names.map(weapon => {
      weapon_options.push({
        value: weapon,
        label: weapon
      })
    })
    return weapon_options
  }

  state = {
    weapon_name: ''
  }

  chooseWeaponLevel = (e) => {
    let btns = document.querySelectorAll('.weapon_level')
    this.props.chooselevel(e, btns)
  };

  importedWeapon = () => {
    if (this.props.imported_character()) {
      let btns = document.querySelectorAll('.weapon_level')
      btns.forEach(btn => {
        let b = btn as any
        if (parseInt(b.firstChild.nodeValue) == this.props.imported_character().weapon.level) {
          b.currentTarget = b
          this.chooseWeaponLevel(b)
        }
      })
      this.setState({
        weapon_name: this.props.imported_character().weapon.name
      }, () => {
        this.props.choosename(this.props.imported_character().weapon.name)
      })
    }
  }

  chooseWeaponName = (e) => {
    let name = e.label
    this.setState({
      weapon_name: name
    })
    this.props.choosename(name)
  };

  weaponImg = () => {
    if (this.state.weapon_name != '') {
      let weapon_img = genshindb.weapons(this.state.weapon_name).images.icon;
      resource.preloadImage(weapon_img)
      return (
        <React.Suspense fallback={<div className="weapon_img_suspense"></div>}>
          <Img crossOrigin="anonymous" className="weapon_img" src={weapon_img} alt={'genshin calculator'} />
        </React.Suspense>
      )
    }
    else {
      return (
        <React.Suspense fallback={<div className="weapon_img_suspense"></div>}>
          <Img crossOrigin="anonymous" className="weapon_img" src={this.props.element} alt={'genshin calculator'} />
        </React.Suspense>
      )
    }
  }

  componentDidMount() {
    this.importedWeapon()
  }

  public render(): JSX.Element {
    return (
      <div className="weapon_container">
        <h2>Weapons</h2>
        <div className="weapon_level_img">
          <div className="weapon_levels">
            <div className="levels_container">
              <div className="levels">
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  20
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  40
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  50
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  60
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  70
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  80
                </button>
              </div>
              <div className="levels_ascend">
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  20
                  <img src={Star} />
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  40
                  <img src={Star} />
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  50
                  <img src={Star} />
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  60
                  <img src={Star} />
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  70
                  <img src={Star} />
                </button>
                <button
                  type="button"
                  className="btn btn-dark weapon_level"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  80
                  <img src={Star} />
                </button>
              </div>
              <div className="levels">
                <button
                  type="button"
                  className="btn btn-dark weapon_level weapon_level_90 level_90"
                  onClick={this.chooseWeaponLevel.bind(this)}
                >
                  90
                </button>
              </div>
            </div>
          </div>
          {this.weaponImg()}
        </div>
        <Select
          options={this.weaponOptions()}
          styles={weapon_style}
          onChange={this.chooseWeaponName.bind(this)}
          placeholder={this.props.imported_character() ? this.props.imported_character().weapon.name : 'Choose weapon...'}
        />
      </div>
    );
  }
}