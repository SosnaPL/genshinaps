import React from 'react';
import '../css/talents_container.scss';
import { talent_auto_style, talent_e_style, talent_q_style } from './select_styles';
import { Img } from 'react-suspense-img';
import Select from 'react-select';

interface TalentsProps {
  auto: string,
  e: string,
  q: string,
  p1: string,
  p2: string,
  up: string,
  talentsLevels: Function,
  imported_character: Function
}

export default class TalentsContainer extends React.Component<TalentsProps> {

  constructor(props) {
    super(props)
  }

  talentOptions = () => {
    let talent_options = []
    Array.from({ length: 15 }, (_, i) => i + 1).map(level => {
      talent_options.push({
        value: level,
        label: level.toString(),
      })
    })
    return talent_options
  }

  chooseLevel = (talent, level) => {
    this.props.talentsLevels(talent, level)
  }

  public render(): JSX.Element {
    return (
      <>
        <h2>Talents</h2>
        <div className="skills">
          <div className="skills_images">
            <React.Suspense fallback={<div className="img_suspense"></div>}>
              <Img crossOrigin="anonymous" className="auto" src={this.props.auto} alt={'genshin calculator'} />
            </React.Suspense>
            <React.Suspense fallback={<div className="img_suspense"></div>}>
              <Img crossOrigin="anonymous" className="e" src={this.props.e} alt={'genshin calculator'} />
            </React.Suspense>
            <React.Suspense fallback={<div className="img_suspense"></div>}>
              <Img crossOrigin="anonymous" className="q" src={this.props.q} alt={'genshin calculator'} />
            </React.Suspense>
          </div>
          <div className="skills_levels">
            <Select
              options={this.talentOptions()}
              styles={talent_auto_style}
              defaultValue={1}
              placeholder={this.props.imported_character() ? this.props.imported_character().auto.value : '1'}
              isSearchable={false}
              onChange={this.chooseLevel.bind(this, 'auto')}
            />
            <Select
              options={this.talentOptions()}
              styles={talent_e_style}
              defaultValue={1}
              placeholder={this.props.imported_character() ? this.props.imported_character().e.value : '1'}
              isSearchable={false}
              onChange={this.chooseLevel.bind(this, 'e')}
            />
            <Select
              options={this.talentOptions()}
              styles={talent_q_style}
              defaultValue={1}
              placeholder={this.props.imported_character() ? this.props.imported_character().q.value : '1'}
              isSearchable={false}
              onChange={this.chooseLevel.bind(this, 'q')}
            />
          </div>
        </div>
        <div className="passives">
          <React.Suspense fallback={<div className="img_suspense"></div>}>
            <Img crossOrigin="anonymous" className="p1" src={this.props.p1} alt={'genshin calculator'} />
          </React.Suspense>
          <React.Suspense fallback={<div className="img_suspense"></div>}>
            <Img crossOrigin="anonymous" className="p2" src={this.props.p2} alt={'genshin calculator'} />
          </React.Suspense>
          <React.Suspense fallback={<div className="img_suspense"></div>}>
            <Img crossOrigin="anonymous" className="up" src={this.props.up} alt={'genshin calculator'} />
          </React.Suspense>
        </div>
      </>
    );
  }
}