import React from 'react';
import Loading from '../public/loading.gif';
import '../css/404.scss';

export default class NotFound extends React.Component {
  public render(): JSX.Element {
    return (
      <>
        <div className="suspense">
          <h1>404 Not Found</h1>
          <img src={Loading} />
        </div>
      </>
    );
  }
}