import genshindb from '../../libraries/genshindb.js';

export const ganyu = {
  auto: {
    labels: genshindb.talents('ganyu').combat1.attributes.labels.slice(0, 8),
    dmg: Object.entries(genshindb.talents('ganyu').combat1.attributes.parameters).slice(0, 8).map(entry => entry[1])
  },
  e: {
    labels: genshindb.talents('ganyu').combat2.attributes.labels[1],
    dmg: genshindb.talents('ganyu').combat1.attributes.parameters.param2
  },
  q: {
    labels: genshindb.talents('ganyu').combat3.attributes.labels[0],
    dmg: genshindb.talents('ganyu').combat3.attributes.parameters.param1
  }
}