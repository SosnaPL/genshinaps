
export enum Weapon_types {
    sword = "sword",
    claymore = "claymore",
    polearm = "polearm",
    catalyst = "catalyst",
    bow = "bow"
}

export enum Elements {
    pyro = "pyro",
    hydro = "hydro",
    anemo = "anemo",
    electro = "electro",
    cryo = "cryo",
    geo = "geo"
}

export const elements_images = {
    'pyro': require('../public/elements/pyro.png').default,
    'hydro': require('../public/elements/hydro.png').default,
    'anemo': require('../public/elements/anemo.png').default,
    'electro': require('../public/elements/electro.png').default,
    'cryo': require('../public/elements/cryo.png').default,
    'geo': require('../public/elements/geo.png').default,
}

export const weapon_types_images = {
    'sword': require('../public/weapon_types/sword.png').default,
    'claymore': require('../public/weapon_types/claymore.png').default,
    'polearm': require('../public/weapon_types/polearm.png').default,
    'catalyst': require('../public/weapon_types/catalyst.png').default,
    'bow': require('../public/weapon_types/bow.png').default,
}

export const talent_scaling = {
    'normal_attack': {
        '1': 100,
        '2': 108,
        '3': 116,
        '4': 127.5,
        '5': 135,
        '6': 145,
        '7': 157.5,
        '8': 170,
        '9': 182.5,
        '10': 197.5,
        '11': 212.5,
        '12': 212.5,
        '13': 212.5,
        '14': 212.5,
        '15': 212.5,
    },
    'flat': {
        '1': 100,
        '2': 110,
        '3': 120,
        '4': 132.5,
        '5': 145,
        '6': 157.5,
        '7': 172.5,
        '8': 187.5,
        '9': 202.5,
        '10': 220,
        '11': 237.5,
        '12': 255,
        '13': 275,
        '14': 275,
        '15': 275,
    },
    'percentage': {
        '1': 100,
        '2': 107.5,
        '3': 115,
        '4': 125,
        '5': 132.5,
        '6': 140,
        '7': 150,
        '8': 160,
        '9': 170,
        '10': 180,
        '11': 190,
        '12': 200,
        '13': 212.5,
        '14': 212.5,
        '15': 212.5,
    }

}

export let characters = {
    'albedo': {
        weapon: Weapon_types.sword,
        element: Elements.geo
    },
    'aloy': {
        weapon: Weapon_types.bow,
        element: Elements.cryo
    },
    'amber': {
        weapon: Weapon_types.bow,
        element: Elements.pyro
    },
    'ayaka': {
        weapon: Weapon_types.sword,
        element: Elements.cryo
    },
    'barbara': {
        weapon: Weapon_types.catalyst,
        element: Elements.hydro
    },
    'beidou': {
        weapon: Weapon_types.claymore,
        element: Elements.electro,
    },
    'bennett': {
        weapon: Weapon_types.sword,
        element: Elements.pyro
    },
    'chongyun': {
        weapon: Weapon_types.claymore,
        element: Elements.cryo
    },
    'diluc': {
        weapon: Weapon_types.claymore,
        element: Elements.pyro
    },
    'diona': {
        weapon: Weapon_types.bow,
        element: Elements.cryo
    },
    'eula': {
        weapon: Weapon_types.claymore,
        element: Elements.cryo
    },
    'fischl': {
        weapon: Weapon_types.bow,
        element: Elements.electro
    },
    'ganyu': {
        weapon: Weapon_types.bow,
        element: Elements.cryo,
    },
    'hutao': {
        weapon: Weapon_types.polearm,
        element: Elements.pyro
    },
    'jean': {
        weapon: Weapon_types.sword,
        element: Elements.anemo
    },
    'kaeya': {
        weapon: Weapon_types.sword,
        element: Elements.cryo
    },
    'kazuha': {
        weapon: Weapon_types.sword,
        element: Elements.anemo
    },
    'keqing': {
        weapon: Weapon_types.sword,
        element: Elements.electro
    },
    'klee': {
        weapon: Weapon_types.catalyst,
        element: Elements.pyro
    },
    'lisa': {
        weapon: Weapon_types.catalyst,
        element: Elements.electro
    },
    'mona': {
        weapon: Weapon_types.catalyst,
        element: Elements.hydro
    },
    'ningguang': {
        weapon: Weapon_types.catalyst,
        element: Elements.geo
    },
    'noelle': {
        weapon: Weapon_types.claymore,
        element: Elements.geo
    },
    'qiqi': {
        weapon: Weapon_types.sword,
        element: Elements.cryo
    },
    'raiden': {
        weapon: Weapon_types.sword,
        element: Elements.electro
    },
    'razor': {
        weapon: Weapon_types.claymore,
        element: Elements.electro
    },
    'rosaria': {
        weapon: Weapon_types.polearm,
        element: Elements.cryo
    },
    'sara': {
        weapon: Weapon_types.bow,
        element: Elements.electro
    },
    'sayu': {
        weapon: Weapon_types.claymore,
        element: Elements.anemo
    },
    'sucrose': {
        weapon: Weapon_types.catalyst,
        element: Elements.anemo
    },
    'tartaglia': {
        weapon: Weapon_types.bow,
        element: Elements.hydro
    },
    'venti': {
        weapon: Weapon_types.bow,
        element: Elements.anemo
    },
    'xiangling': {
        weapon: Weapon_types.polearm,
        element: Elements.pyro
    },
    'xiao': {
        weapon: Weapon_types.polearm,
        element: Elements.anemo
    },
    'xingqiu': {
        weapon: Weapon_types.sword,
        element: Elements.hydro
    },
    'xinyan': {
        weapon: Weapon_types.claymore,
        element: Elements.pyro
    },
    'yanfei': {
        weapon: Weapon_types.catalyst,
        element: Elements.pyro
    },
    'yoimiya': {
        weapon: Weapon_types.bow,
        element: Elements.pyro
    },
    'zhongli': {
        weapon: Weapon_types.polearm,
        element: Elements.geo
    },
}