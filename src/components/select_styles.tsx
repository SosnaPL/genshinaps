export const weapon_style = {
    container: (styles) => ({
        width: '500px',
        "@media only screen and (max-width: 550px)": {
            ...styles["@media only screen and (max-width: 550px)"],
            width: '100%'
        },
    }),
    singleValue: styles => ({
        ...styles,
        color: '$fontColor'
    }),
    input: styles => ({
        ...styles,
        color: '$fontColor'
    }),
    control: styles => ({
        ...styles,
        backgroundColor: 'rgba(16, 15, 20, 0.5)',
        color: '$fontColor',
        borderRadius: 8,
        boxShadow: 'none',
        borderColor: 'transparent',
        ":hover": {
            backgroundColor: 'rgba(16, 15, 20, 0.5)',
            boxShadow: 'none',
            borderColor: 'transparent',
        }
    }),
    menu: (provided, state) => ({
        ...provided,
        width: '500px',
        color: state.selectProps.menuColor,
        borderRadius: 8,
        top: 'calc(100% - 20px)',
        backgroundColor: 'rgb(16, 15, 20)',
        "@media only screen and (max-width: 550px)": {
            top: 'calc(100% - 45px)',
            width: 'calc(100% - 10px)'
        },
    }),
    option: (provided) => ({
        ...provided,
        display: 'flex',
        minWidth: '100%',
        color: '$fontColor',
        borderRadius: 15,
        background: 'transparent',
        transition: 'background .2s ease-in-out',
        ':hover': {
            background: 'var(--color)',
        },
    }),
}

export const talent_auto_style = {
    container: () => ({
        width: '100%',
        margin: 21
    }),
    singleValue: styles => ({
        ...styles,
        color: '$fontColor'
    }),
    input: styles => ({
        ...styles,
        color: '$fontColor'
    }),
    control: styles => ({
        ...styles,
        backgroundColor: 'rgba(16, 15, 20, 0.5)',
        color: '$fontColor',
        borderRadius: 8,
        boxShadow: 'none',
        borderColor: 'transparent',
        ":hover": {
            backgroundColor: 'rgba(16, 15, 20, 0.5)',
            boxShadow: 'none',
            borderColor: 'transparent',
        }
    }),
    menu: (provided, state) => ({
        ...provided,
        width: '100%',
        top: 55,
        position: 'absolute',
        color: state.selectProps.menuColor,
        borderRadius: 8,
        backgroundColor: 'rgb(16, 15, 20)'
    }),
    option: (provided) => ({
        ...provided,
        display: 'flex',
        minWidth: '100%',
        color: '$fontColor',
        borderRadius: 15,
        background: 'transparent',
        transition: 'background .2s ease-in-out',
        ':hover': {
            background: 'var(--color)',
        },
    }),
}
export const talent_e_style = {
    container: () => ({
        width: '100%',
        margin: 21
    }),
    singleValue: styles => ({
        ...styles,
        color: '$fontColor'
    }),
    input: styles => ({
        ...styles,
        color: '$fontColor'
    }),
    control: styles => ({
        ...styles,
        backgroundColor: 'rgba(16, 15, 20, 0.5)',
        color: '$fontColor',
        borderRadius: 8,
        boxShadow: 'none',
        borderColor: 'transparent',
        ":hover": {
            backgroundColor: 'rgba(16, 15, 20, 0.5)',
            boxShadow: 'none',
            borderColor: 'transparent',
        }
    }),
    menu: (provided, state) => ({
        ...provided,
        width: '100%',
        top: 135,
        position: 'absolute',
        color: state.selectProps.menuColor,
        borderRadius: 8,
        backgroundColor: 'rgb(16, 15, 20)'
    }),
    option: (provided) => ({
        ...provided,
        display: 'flex',
        minWidth: '100%',
        color: '$fontColor',
        borderRadius: 8,
        background: 'transparent',
        transition: 'background .2s ease-in-out',
        ':hover': {
            background: 'var(--color)',
        },
    }),
}
export const talent_q_style = {
    container: () => ({
        width: '100%',
        margin: 21
    }),
    singleValue: styles => ({
        ...styles,
        color: '$fontColor'
    }),
    input: styles => ({
        ...styles,
        color: '$fontColor'
    }),
    control: styles => ({
        ...styles,
        backgroundColor: 'rgba(16, 15, 20, 0.5)',
        color: '$fontColor',
        borderRadius: 8,
        boxShadow: 'none',
        borderColor: 'transparent',
        ":hover": {
            backgroundColor: 'rgba(16, 15, 20, 0.5)',
            boxShadow: 'none',
            borderColor: 'transparent',
        }
    }),
    menu: (provided, state) => ({
        ...provided,
        width: '100%',
        top: 215,
        position: 'absolute',
        color: state.selectProps.menuColor,
        borderRadius: 8,
        backgroundColor: 'rgb(16, 15, 20)'
    }),
    option: (provided) => ({
        ...provided,
        display: 'flex',
        minWidth: '100%',
        color: '$fontColor',
        borderRadius: 8,
        background: 'transparent',
        transition: 'background .2s ease-in-out',
        ':hover': {
            background: 'var(--color)',
        },
    }),
}