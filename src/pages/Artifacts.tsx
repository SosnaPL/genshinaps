import React from 'react';
import '../css/artifacts.scss';
import { Helmet } from "react-helmet";

export default class Artifacts extends React.Component {
  public render(): JSX.Element {
    return (
      <div className="artifacts">
        <Helmet>
          <meta charSet="utf-8" name='keywords' content='genshin apps calculator artifacts damage' />
          <title>Genshin Artifacts Simulator</title>
          <meta name="description" content="Genshin Impact artifacts simulator." />
          <link rel="canonical" href="http://genshinapps.netlify.app/#/artifacts" />
        </Helmet>
      </div>
    );
  }
}