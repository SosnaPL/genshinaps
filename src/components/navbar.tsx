import React from 'react';
//import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../css/navbar.scss';
import Logo from '../public/logo.png';

export default class Navbar extends React.Component {
  public render(): JSX.Element {
    return (
      <div className="navbar">
        <div className="logo">
          <img src={Logo} alt="genshin apps" />
        </div>
        <div className="tabs">
          <div className="calculator_link">
            <Link to="/calculator">
              <h1>Calculator</h1>
            </Link>
          </div>
          <div className="artifacts_link">
            <Link to="/artifacts">
              <h1>Artifacts</h1>
            </Link>
          </div>
          <div className="credits_link">
            <Link to="/credits">
              <h1>About</h1>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
