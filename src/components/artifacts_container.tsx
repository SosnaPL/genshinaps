import React from 'react';
import '../css/artifacts_container.scss';
import { Row } from 'react-bootstrap';

export default class ArtifactsContainer extends React.Component<{ inputs: Function, imported_character: Function }> {

  constructor(props) {
    super(props);
  }

  artifactsDetails = () => {
    let inputs = document.querySelectorAll('.form__field')
    this.props.inputs(inputs)
  };

  importedArtifacts = () => {
    if (this.props.imported_character()) {
      let inputs = document.querySelectorAll('.form__field')
      let artifacts = Object.keys(this.props.imported_character().artifacts)
      inputs.forEach((input, index) => {
        let i = input as any
        if (i.placeholder.includes('%')) {
          i.value = this.props.imported_character().artifacts[artifacts[index]] * 100
        }
        else {
          i.value = this.props.imported_character().artifacts[artifacts[index]]
        }
      })
    }
  }

  componentDidMount() {
    this.importedArtifacts()
  }

  public render(): JSX.Element {
    return (
      <div className="artifacts_container">
        <h2>Artifacts</h2>
        <Row>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="ATK" id='atk' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="atk" className="form__label">ATK</label>
          </div>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="ATK%" id='atk%' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="atk%" className="form__label">ATK%</label>
          </div>
        </Row>
        <Row>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="HP" id='hp' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="hp" className="form__label">HP</label>
          </div>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="HP%" id='hp%' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="hp%" className="form__label">HP%</label>
          </div>
        </Row>
        <Row>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="DEF" id='def' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="def" className="form__label">DEF</label>
          </div>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="DEF%" id='def%' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="def%" className="form__label">DEF%</label>
          </div>
        </Row>
        <Row>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="Crit RATE" id='cr' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="cr" className="form__label">Crit RATE</label>
          </div>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="Crit DMG" id='cd' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="cd" className="form__label">Crit DMG</label>
          </div>
        </Row>
        <Row>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="Physical DMG" id='phy' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="phy" className="form__label">Physical DMG</label>
          </div>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="Elemental DMG" id='ele' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="ele" className="form__label">Elemental DMG</label>
          </div>
        </Row>
        <Row>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="Physical DMG" id='ele_m' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="ele_m" className="form__label">Elemental Mastery</label>
          </div>
          <div className="form__group field">
            <input type="number" className="form__field" placeholder="Elemental DMG" id='energy_rc' onChange={this.artifactsDetails.bind(this)} />
            <label htmlFor="energy_rc" className="form__label">Energy Recharge</label>
          </div>
        </Row>
      </div>
    );
  }
}