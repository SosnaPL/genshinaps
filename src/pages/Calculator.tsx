import React from 'react';
import Characters from '../components/characters';
import '../css/calculator.scss';

export default class Calculator extends React.Component {
  public render(): JSX.Element {
    return (
      <div className="calculator">
        <Characters />
      </div>
    );
  }
}