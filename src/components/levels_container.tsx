import React from 'react';
import Star from "../public/star.png";
import '../css/levels_container.scss'
export default class LevelsContainer extends React.Component<{ choose: Function, imported_character: Function }> {

  constructor(props) {
    super(props);
  }

  chooseLevel = (e) => {
    let btns = document.querySelectorAll('.character_level')
    this.props.choose(e, btns)
  };

  importedLevel = () => {
    if (this.props.imported_character()) {
      let btns = document.querySelectorAll('.character_level')
      btns.forEach(btn => {
        let b = btn as any
        if (parseInt(b.firstChild.nodeValue) == this.props.imported_character().level) {
          b.currentTarget = b
          this.chooseLevel(b)
        }
      })
    }
  }

  componentDidMount() {
    this.importedLevel()
  }

  public render(): JSX.Element {
    return (
      <>
        <h2>Levels</h2>
        <div className="levels_container">
          <div className="levels">
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              20
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              40
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              50
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              60
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              70
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              80
            </button>
          </div>
          <div className="levels_ascend">
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              20
              <img src={Star} alt={'genshin calculator'} />
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              40
              <img src={Star} alt={'genshin calculator'} />
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              50
              <img src={Star} alt={'genshin calculator'} />
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              60
              <img src={Star} alt={'genshin calculator'} />
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              70
              <img src={Star} alt={'genshin calculator'} />
            </button>
            <button
              type="button"
              className="btn btn-dark character_level"
              onClick={this.chooseLevel.bind(this)}
            >
              80
              <img src={Star} alt={'genshin calculator'} />
            </button>
          </div>
          <div className="levels">
            <button
              type="button"
              className="btn btn-dark character_level level_90"
              onClick={this.chooseLevel.bind(this)}
            >
              90
            </button>
          </div>
        </div>
      </>
    );
  }
}