import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import '../css/characters.scss';
import { withRouter, RouteComponentProps } from "react-router";
import { Img, resource } from 'react-suspense-img';
import { characters } from "./characters_list";
import genshindb from '../libraries/genshindb.js';

declare const window: any;

class Characters extends React.Component<RouteComponentProps> {

  constructor(props) {
    super(props);
  }

  state = {
    stable_characters: Object.keys(characters),
    characters: Object.keys(characters)
  }

  onChange(e) {
    let name = e.target.value.toString().toLowerCase();
    console.log(name)
    let characters = this.state.stable_characters;
    let characters_filtered = characters.filter((character) => {
      return character.toLowerCase().includes(name)
    })
    this.setState({ characters: characters_filtered })
  }

  character(char) {
    window.gtag("event", char, {
      event_category: "Character",
    });
    document.documentElement.style.setProperty('--border', "solid 5px transparent")
    document.documentElement.style.setProperty('--trans', 'scaleX(0)')
    this.props.history.push("/character?" + char);
  }

  public render(): JSX.Element {
    return (
      <div className="characters_container">
        <div className="search_character">
          <FontAwesomeIcon size="2x" icon={faSearch} className="search_icon" />
          <input className="search_input" type="text" spellCheck="false" onChange={this.onChange.bind(this)} />
        </div>
        <div className="characters">
          {this.state.characters.map((char, id) => {
            resource.preloadImage(genshindb.characters(char).images.icon)
            return (
              <React.Suspense key={id} fallback={<div className="character_suspense"></div>}>
                <div className="character" key={id}>
                  <Img crossOrigin="anonymous" onClick={this.character.bind(this, char)} src={genshindb.characters(char).images.icon} alt={'genshin calculator'} />
                </div>
              </React.Suspense>
            )
          })}
        </div>
      </div>
    );
  }
}

export default withRouter(Characters);