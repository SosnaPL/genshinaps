import React from 'react';
import '../css/dmg_container.scss';

interface DmgProps {
  hp: number;
  atk: number;
  def: number;
  crit_rate: number;
  crit_dmg: number;
  phy_dmg: number;
  ele_dmg: number;
  ele_m: number;
  energy_rc: number;
  auto: number;
  e: number;
  q: number;
  name: string;
}

export default class DmgContainer extends React.Component<DmgProps> {

  constructor(props: DmgProps) {
    super(props);
  }

  isValid = require('./characters_dmg/characters')[this.props.name] ? true : false
  auto = this.isValid ? require('./characters_dmg/characters')[this.props.name].auto : '';
  e = this.isValid ? require('./characters_dmg/characters')[this.props.name].e : '';
  q = this.isValid ? require('./characters_dmg/characters')[this.props.name].q : '';

  autoDetails = () => {
    return (
      this.auto.labels.map((label, id) => {
        let skill_dmg = Math.floor(this.props.atk * this.auto.dmg[id][this.props.auto - 1] * (1 + (this.props.ele_dmg / 100)))
        return (
          <div key={id}>{label.split('|')[0]} : {skill_dmg} </div>
        )
      })
    )
  }

  eDetails = () => {
    let skill_dmg = Math.floor(this.props.atk * this.e.dmg[this.props.auto - 1] * (1 + (this.props.ele_dmg / 100)))
    return (
      <div>{this.e.labels.split('|')[0]} : {skill_dmg} </div>
    )
  }

  qDetails = () => {
    let skill_dmg = Math.floor(this.props.atk * this.q.dmg[this.props.auto - 1] * (1 + (this.props.ele_dmg / 100)))
    return (
      <div>{this.q.labels.split('|')[0]} : {skill_dmg} </div>
    )
  }

  componentDidMount() {
  }

  public render(): JSX.Element {
    return (
      <div className="dmg_container">
        <div className="auto_dmg">
          <h4>Auto</h4>
          {this.isValid ? this.autoDetails() : ''}
        </div>
        <div className="e_dmg">
          <h4>Elemental Skill</h4>
          {this.isValid ? this.eDetails() : ''}
        </div>
        <div className="q_dmg">
          <h4>Elemental Burst</h4>
          {this.isValid ? this.eDetails() : ''}
        </div>
      </div>
    );
  }
}