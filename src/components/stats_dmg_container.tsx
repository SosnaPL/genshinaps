import React from 'react';
import '../css/stats_dmg_container.scss';
import genshindb from '../libraries/genshindb.js';
import DmgContainer from './dmg_container';

export default class StatsDmgContainer extends React.Component<{ details: any, character: string }> {

  constructor(props) {
    super(props);
  }

  details = () => {
    let substat = genshindb.characters(this.props.character).substat
    let weapon_stat = genshindb.weapons(this.props.details.weapon.name) ? genshindb.weapons(this.props.details.weapon.name).substat : ''
    let char = genshindb.characters(this.props.character).stats(this.props.details.level, this.props.details.ascend)
    let weapon = genshindb.weapons(this.props.details.weapon.name) ? genshindb.weapons(this.props.details.weapon.name).stats(this.props.details.weapon.level, this.props.details.weapon.ascend) : ''
    let details = {
      character: char,
      character_substat: substat,
      weapon_substat: weapon_stat,
      weapon: weapon,
      artifacts: this.props.details.artifacts,
      auto: this.props.details.auto.value,
      e: this.props.details.e.value,
      q: this.props.details.q.value,
    }
    return details
  }

  hp = () => {
    let weapon_specialization = this.details().weapon_substat == "HP" ? this.details().weapon.specialized : 0
    let specialization = this.details().character_substat == "HP" ? this.details().character.specialized : 0
    return Math.round(this.details().character.hp + (this.details().character.hp * (this.details().artifacts.hp_p + specialization + weapon_specialization)) + this.details().artifacts.hp)
  }

  atk = () => {
    let weapon_specialization = this.details().weapon_substat == "ATK" ? this.details().weapon.specialized : 0
    let specialization = this.details().character_substat == "ATK" ? this.details().character.specialized : 0
    let base_atk = this.details().character.attack + (this.details().weapon ? this.details().weapon.attack : 0)
    return Math.round((base_atk * (1 + this.details().artifacts.atk_p + specialization + weapon_specialization)) + this.details().artifacts.atk)
  }

  def = () => {
    let weapon_specialization = this.details().weapon_substat == "DEF" ? this.details().weapon.specialized : 0
    let specialization = this.details().character_substat == "DEF" ? this.details().character.specialized : 0
    return Math.round(this.details().character.defense + (this.details().character.defense * (this.details().artifacts.def_p + specialization + weapon_specialization)) + this.details().artifacts.def)
  }

  crit_rate = () => {
    let weapon_specialization = this.details().weapon_substat == "CRIT Rate" ? Math.round(this.details().weapon.specialized * 100) : 0
    let specialization = this.details().character_substat == "CRIT Rate" ? Math.round(this.details().character.specialized * 100) : 5
    return this.details().artifacts.crit_rate + specialization + weapon_specialization
  }

  crit_dmg = () => {
    let weapon_specialization = this.details().weapon_substat == "CRIT DMG" ? Math.round(this.details().weapon.specialized * 100) : 0
    let specialization = this.details().character_substat == "CRIT DMG" ? Math.round(this.details().character.specialized * 100) : 50
    return this.details().artifacts.crit_dmg + specialization + weapon_specialization
  }

  phy_dmg = () => {
    let specialization = this.details().character_substat.includes('Physical') ? Math.round(this.details().character.specialized * 100) : 0
    let weapon_specialization = this.details().weapon_substat.includes('Physical') ? Math.round(this.details().weapon.specialized * 100) : 0
    return this.details().artifacts.phy_dmg + weapon_specialization + specialization
  }

  ele_dmg = () => {
    let elements = ["Pyro",
      "Hydro",
      "Anemo",
      "Electro",
      "Cryo",
      "Geo"]
    let specialization = 0
    elements.forEach(element => {
      if (this.details().character_substat.includes(element)) {
        specialization = Math.round(this.details().character.specialized * 100)
      }
    })
    return this.details().artifacts.ele_dmg + specialization
  }

  ele_m = () => {
    let weapon_specialization = this.details().weapon_substat == "Elemental Mastery" ? Math.round(this.details().weapon.specialized) : 0
    let specialization = this.details().character_substat == "Elemental Mastery" ? Math.round(this.details().character.specialized) : 0
    return this.details().artifacts.ele_m + specialization + weapon_specialization
  }

  energy_rc = () => {
    let weapon_specialization = this.details().weapon_substat == "Energy Recharge" ? Math.round(this.details().weapon.specialized * 100) : 0
    let specialization = this.details().character_substat == "Energy Recharge" ? Math.round(this.details().character.specialized * 100) : 0
    return this.details().artifacts.energy_rc + specialization + weapon_specialization
  }

  componentDidMount() {
  }

  public render(): JSX.Element {
    return (
      <div className="stats_dmg_container">
        <div className="details_container">
          <h2>Details</h2>
          <div className="details_row">
            <div className="details_column">
              <div className="details_stat">
                <h4>Max HP</h4>
              </div>
              <div className="details_stat">
                <h4>ATK</h4>
              </div>
              <div className="details_stat">
                <h4>DEF</h4>
              </div>
              <div className="details_stat">
                <h4>Crit RATE</h4>
              </div>
              <div className="details_stat">
                <h4>Crit DMG</h4>
              </div>
              <div className="details_stat">
                <h4>Physical DMG</h4>
              </div>
              <div className="details_stat">
                <h4>Elemental DMG</h4>
              </div>
              <div className="details_stat">
                <h4>Elemental Mastery</h4>
              </div>
              <div className="details_stat">
                <h4>Energy Recharge</h4>
              </div>
            </div>
            <div className="details_column">
              <div className="details_value">{this.hp()}</div>
              <div className="details_value">{this.atk()}</div>
              <div className="details_value">{this.def()}</div>
              <div className="details_value">{this.crit_rate()}</div>
              <div className="details_value">{this.crit_dmg()}</div>
              <div className="details_value">{this.phy_dmg()}</div>
              <div className="details_value">{this.ele_dmg()}</div>
              <div className="details_value">{this.ele_m()}</div>
              <div className="details_value">{this.energy_rc()}</div>
            </div>
          </div>
        </div>
        <DmgContainer hp={this.hp()} atk={this.atk()} def={this.def()} crit_rate={this.crit_rate()} crit_dmg={this.crit_dmg()} phy_dmg={this.phy_dmg()} ele_dmg={this.ele_dmg()} ele_m={this.ele_m()} energy_rc={this.energy_rc()} auto={this.details().auto} e={this.details().e} q={this.details().q} name={this.props.character} />
      </div>
    );
  }
}