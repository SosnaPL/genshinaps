import React from 'react';
import '../css/credits.scss';

export default class Credits extends React.Component {
  public render(): JSX.Element {
    return (
      <div className="about">
        <div className="heading">
          <h3>Fan made Genshin Impact calculator that I'm working on after work</h3>
        </div>
        <div className="description">
          <div className="working">
            <h3>Working on</h3>
            <ul>
              <li>
                <p>Calculating overall dmg</p>
              </li>
              <li>
                <p>Individual calculations for each character</p>
              </li>
              <li>
                <p>Buffs tab</p>
              </li>
            </ul>
          </div>
          <div className="done">
            <h3>Done</h3>
            <ul>
              <li>
                <p>Character talents</p>
              </li>
              <li>
                <p>Character levels</p>
              </li>
              <li>
                <p>Weapons details</p>
              </li>
              <li>
                <p>Artifacts statistics</p>
              </li>
              <li>
                <p>Stats details</p>
              </li>
              <li>
                <p>Exports/Imports character details</p>
              </li>
            </ul>
          </div>
        </div>
        <div className="more">
          <a className='btn more_btn' href="https://jsosinski.netlify.app/#/" target="_blank">More...</a>
        </div>
      </div>
    );
  }
}