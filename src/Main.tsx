import React from 'react';
import Navbar from './components/navbar';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { Helmet } from "react-helmet";
import Loading from './public/loading.gif';

const Artifacts = React.lazy(() => import('./pages/Artifacts'));
const Calculator = React.lazy(() => import('./pages/Calculator'));
const Character = React.lazy(() => import('./components/character'));
const NotFound = React.lazy(() => import('./pages/404'));
const Credits = React.lazy(() => import('./pages/Credits'));

export default class Main extends React.Component {

  componentDidMount() {
    let scrollup = this.refs.scroll as HTMLButtonElement;
    scrollup.addEventListener("click", _e => {
      let currentYOffset = self.pageYOffset;
      let initYOffset = currentYOffset;

      var intervalId = setInterval(function () {
        currentYOffset -= initYOffset * 0.1;
        document.body.scrollTop = currentYOffset;
        document.documentElement.scrollTop = currentYOffset;

        if (self.pageYOffset == 0) {
          clearInterval(intervalId);
        }
      }, 20);
    })
    let scrollFunction = () => {
      if (
        document.body.scrollTop > 20 ||
        document.documentElement.scrollTop > 20
      ) {
        scrollup.style.visibility = "visible";
        scrollup.style.cursor = "pointer";
        scrollup.style.opacity = "1";
        scrollup.disabled = false;
      } else {
        scrollup.style.opacity = "0";
        scrollup.style.cursor = "default";
        scrollup.disabled = true;
      }
    }
    window.onscroll = function () {
      scrollFunction();
    };
  }

  public render(): JSX.Element {
    return (
      <div className="app">
        <Helmet>
          <meta charSet="utf-8" name='keywords' content='genshin apps calculator artifacts damage' />
          <title>Genshin Apps</title>
          <meta name="description" content="Genshin Impact damage calculator and artifacts simulator." />
          <link rel="canonical" href="http://genshinapps.netlify.app/" />
        </Helmet>
        <Router>
          <Navbar />
          <React.Suspense
            fallback={(
              <div className="suspense">
                <img src={Loading} />
              </div>
            )}
          >
            <Switch>
              <Route path="/calculator" component={Calculator} />
              <Route path="/artifacts" component={Artifacts} />
              <Route path="/character" component={Character} />
              <Route path="/credits" component={Credits} />
              <Route path="/404" component={NotFound} />
              <Route path='/' exact={true} component={Calculator} />
              <Route path='*' exact={true} component={NotFound} />
            </Switch>
          </React.Suspense>
        </Router>
        <button ref={"scroll"} type="button" className="scrollup btn btn-lg">
          <FontAwesomeIcon size="1x" icon={faArrowUp} />
        </button>
      </div>
    );
  }
}
